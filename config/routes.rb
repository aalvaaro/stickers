Rails.application.routes.draw do
  root 'landing#index'

  mount Shoppe::Engine => "/shoppe"
end
